let component = ReasonReact.statelessComponent("DogBreed");

let make = (_children, ~breed) => {
  ...component,
  render: _ => {
    <DogBreedFeed breed>
      ...{data =>
        switch (data##dog) {
        | None => ReasonReact.string("no dog")
        | Some(dog) =>
          switch (dog##images) {
          | Some(images) =>
            images
            |> Js.Array.map(imageElement =>
                 switch (imageElement) {
                 | Some(aElementForSho) => <img src=aElementForSho##url />
                 | None => ReasonReact.string("no image")
                 }
               )
            |> ReasonReact.array
          | None => ReasonReact.string("no images")
          }
        }
      }
    </DogBreedFeed>;
  },
};
