// https://dog-graphql-api.glitch.me/graphql

// Create an InMemoryCache
let inMemoryCache = ApolloInMemoryCache.createInMemoryCache();

// Create an HTTP Link
let httpLink =
  ApolloLinks.createHttpLink(
    ~uri="https://dog-graphql-api.glitch.me/graphql",
    (),
  );

let instance =
  ReasonApollo.createApolloClient(~link=httpLink, ~cache=inMemoryCache, ());