open Css;

global("body", [margin(px(0))]);

/*
 When changing to a bar  you might want to change
 the direction to row instead of column
 */
let appBar =
  style([
    display(flexBox),
    flexDirection(column),
    justifyContent(center),
    flexWrap(wrap),
    backgroundColor(hex("1799E5")),
  ]);
let appContainer =
  style([
    display(flexBox),
    flexDirection(column),
    alignItems(center),
    justifyContent(center),
    flexWrap(wrap),
  ]);
let appItem = style([display(flexBox)]);

let appTitle =
  style([fontFamily("Roboto"), fontSize(em(3.5)), color(hex("000"))]);
let appSubTitle =
  style([fontFamily("Roboto"), fontSize(em(2.5)), color(hex("000"))]);

let card =
  style([
    display(flexBox),
    flexDirection(column),
    justifyContent(spaceBetween),
    alignItems(center),
    backgroundColor(white),
    boxShadow(~y=px(3), ~blur=px(5), rgba(0, 0, 0, 0.3)),
    padding2(~v=px(5), ~h=px(10)),
    margin2(~v=px(5), ~h=px(10)),
    borderRadius(px(8)),
  ]);
let cardTitle = style([display(flexBox)]);
let cardBody = style([display(flexBox)]);
let cardImage =
  style([width(px(250)), height(px(250)), borderRadius(px(8))]);

let dogListLoading = style([]);
let dogListError = style([]);
