/*
 Here we are using the grapqhl PPX which generates
 a module containing the data type of the return value
 the variables needed and query component

 Read more here:https://github.com/mhallin/graphql_ppx
 */
module Dogs = [%graphql
  {|
  query Dogs {
    dogs {
        id
        breed
        displayImage
    }
  }
  |}
];

module GetDogs = ReasonApollo.CreateQuery(Dogs);

let component = ReasonReact.statelessComponent("DogBreedsFeed");

let make = children => {
  ...component,
  render: _ => {
    <GetDogs>
      ...{({result}) =>
        switch (result) {
        | Loading =>
          <div className=Styles.dogListLoading>
            {ReasonReact.string("Loading")}
          </div>
        | Error(error) =>
          <div className=Styles.dogListError>
            {ReasonReact.string(error##message)}
          </div>
        | Data(response) =>
          switch (response##dogs) {
          | None => ReasonReact.string("No dogs")
          | Some(dogs) =>
            ReasonReact.array(
              Array.map(
                data =>
                  switch (data) {
                  | Some(data) => children(~data)
                  | None => ReasonReact.string("No dog breed")
                  },
                dogs,
              ),
            )
          }
        }
      }
    </GetDogs>;
  },
};
