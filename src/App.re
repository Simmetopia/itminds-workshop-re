// The routes
type routes =
  | BreedList
  | NotFound
  | Breed(string);
// The reducer actions
type actions =
  | Route(routes);
// The state record type
type state = {currentRoute: routes};

let component = ReasonReact.reducerComponent("App");

let make = _children => {
  ...component,
  // The state the component mounts with
  initialState: () => {
    let initUrl = ReasonReact.Router.dangerouslyGetInitialUrl();

    switch (initUrl.path) {
    | ["breed", breed] => {currentRoute: Breed(breed)}
    | [] => {currentRoute: BreedList}
    | _ => {currentRoute: NotFound}
    };
  },
  // The reducer to manage state updates
  didMount: self => {
    let watcherUrl =
      ReasonReact.Router.watchUrl(routeParams =>
        switch (routeParams.path) {
        | ["breed", breed] => self.send(Route(Breed(breed)))
        | _ => self.send(Route(NotFound))
        }
      );
    self.onUnmount(() => ReasonReact.Router.unwatchUrl(watcherUrl));
  },
  reducer: (action, _state) => {
    switch (action) {
    | Route(newRoute) => ReasonReact.Update({currentRoute: newRoute})
    };
  },
  render: self =>
    <div className=Styles.appContainer>
      <div className=Styles.appItem>
        <h1 className=Styles.appTitle>
          {ReasonReact.string("Awesome ReasonReact App")}
        </h1>
      </div>
      <div>
        <h2 className=Styles.appSubTitle>
          {ReasonReact.string("Functional programming is Fun()")}
        </h2>
      </div>
      {switch (self.state.currentRoute) {
       | BreedList => <DogBreeds />
       | NotFound => <NotFound />
       | Breed(breed) => <DogBreed breed />
       }}
    </div>,
};
