let component = ReasonReact.statelessComponent("DogBreeds");
/*
 Using the function as a child patteren here to get the dogBreed from
 the network layer.

 See more about function as a child here:
 https://medium.com/merrickchristensen/function-as-child-components-5f3920a9ace9

 */
let make = _children => {
  ...component,
  render: _self => {
    <DogBreedsFeed>
      ...{(~data) =>
        <div
          className=Styles.card
          onClick={_event =>
            ReasonReact.Router.push("/breed/" ++ data##breed)
          }>
          {ReasonReact.string(data##breed)}
          {switch (data##displayImage) {
           | None => ReasonReact.string("no dog")
           | Some(src) => <img src />
           }}
        </div>
      }
    </DogBreedsFeed>;
  },
};
