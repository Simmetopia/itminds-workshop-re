let component = ReasonReact.statelessComponent("NotFound");

let make = _children => {
  ...component,
  render: _self => {
    <div className=Styles.appContainer>
      <div className=Styles.appItem>
        <h1 className=Styles.appTitle>
          {"404 not found" |> ReasonReact.string}
        </h1>
      </div>
    </div>;
  },
};
