/*
 Here we are using the grapqhl PPX which generates
 a module containing the data type of the return value
 the variables needed and query component

 Read more here:https://github.com/mhallin/graphql_ppx
 */
module Dog = [%graphql
  {|
  query Dog($breed: String!) {
    dog(breed: $breed) {
      id
      breed
      images {
        id
        url
      }
    }
  }
  |}
];

module GetDog = ReasonApollo.CreateQuery(Dog);

let component = ReasonReact.statelessComponent(__MODULE__);

let make = (children, ~breed) => {
  ...component,
  render: _ => {
    let dog = Dog.make(~breed, ());
    <GetDog variables=dog##variables>
      ...{({result}) =>
        switch (result) {
        | Loading =>
          <div className=Styles.dogListLoading>
            {ReasonReact.string("Loading")}
          </div>
        | Error(error) =>
          <div className=Styles.dogListError>
            {ReasonReact.string(error##message)}
          </div>
        | Data(data) => children(data)
        }
      }
    </GetDog>;
  },
};
